package br.com.dmendonca.validations;

import java.util.Arrays;
import java.util.List;

public class FileValidation {

    private static final String[] EXT = new String[]{"DOC", "TXT"};

    public static boolean execute(final String extension) {
        final List<String> list = Arrays.asList(EXT);
        return list.stream().anyMatch(extension::equalsIgnoreCase);
    }
}
