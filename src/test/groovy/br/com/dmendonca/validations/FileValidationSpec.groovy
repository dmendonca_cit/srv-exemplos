package br.com.dmendonca.validations

import spock.lang.Specification
import spock.lang.Unroll

class FileValidationSpec extends Specification {

    @Unroll
    def "validar extensão: #extension"() {

        when: "o validador verifica o nome do arquivo"
        def isValid = FileValidation.execute(extension)

        then: "retorna o resultado"
        isValid == expected

        where: "valores de entrada são"
        extension | expected
        "TXT"     | true
        "DOC"     | true
        "PPT"     | false
        "PDF"     | false
        "ODT"     | false
    }
}
