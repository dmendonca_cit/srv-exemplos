package br.com.dmendonca.validations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class FileValidationTest {

    @Parameters
    public static Collection<Object[]> data() {
        return asList(new Object[][]{
                {"TXT", true},
                {"DOC", true},
                {"PPT", false},
                {"PDF", false},
                {"ODT", false}
        });
    }

    private final String extension;
    private final boolean isValid;

    public FileValidationTest(final String input, final boolean expected) {
        this.extension = input;
        this.isValid = expected;
    }

    @Test
    public void fileValidationExecute() {
        assertEquals(isValid, FileValidation.execute(extension));
    }
}
