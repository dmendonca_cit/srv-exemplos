#!groovy

pipeline {
    agent any
    options {
        skipDefaultCheckout(true)
    }

    stages {
        stage('Setup') {
            steps {
                checkout scm
                echo """
                    PROJECT_NAME:      ${PROJECT_NAME}
                    VERSION_INCREMENT: ${VERSION_INCREMENT}
                    CURRENT_VERSION:   ${readVersion()}
                """.stripIndent()
            }
        }

        stage('Unit Tests') {
            steps {
                println "Unit Tests"
                sh './gradlew test'
            }
        }

        stage('Component Tests') {
            steps {
                println "Component Tests"
                sh './gradlew componentTest'
            }
        }

        stage('Integration Tests') {
            steps {
                println "Integration Tests"
                sh './gradlew integrationTest'
            }
        }

        stage('Owasp') {
            steps {
                println "Owasp"
                sh './gradlew dependencyCheckAnalyze --info -x test'
            }
        }

        stage('Sonar') {
            steps {
                println "Sonar"
                //withSonarQubeEnv('SonarQube DevOps') {
                    //sh './gradlew jacocoTestReport'
                    //sh './gradlew --info sonarqube -x test'
                //}
            }
        }

        stage('Reports') {
            steps {
                println "Reports"
                //jacoco(exclusionPattern: """
                //**/br/com/**/Application.class
                //""")
            }
        }

        stage('Release Version') {
            steps {
                println "Initial version: ${readVersion()}"
                sh "./gradlew createRelease -Prelease.disableChecks -Prelease.pushTagsOnly -Prelease.versionIncrementer=${VERSION_INCREMENT} --stacktrace"
                sh "./gradlew clean build -x test"
                sh "rm .git/hooks/pre-push"
                sh "./gradlew release -Prelease.disableChecks -Prelease.pushTagsOnly -Prelease.versionIncrementer=${VERSION_INCREMENT} -Prelease.customKeyFile=\"/var/jenkins_home/.ssh//id_rsa\" --stacktrace"
                println "Created version: ${readVersion()}"

                //releasedVersion = readVersion()
                //currentBuild.displayName = getDisplayName()
            }
        }

        stage('Build') {
            steps {
                println "Build"
            }
        }

        stage('Install') {
            steps {
                println "Install"
            }
        }

        stage('Deploy') {
            steps {
                println "Deploy"
            }
        }

    }
}

def readVersion() {
    return sh(script: "./gradlew cV -q -Prelease.quiet", returnStdout: true).trim()
}

def setupSSHKey() {
    withCredentials([sshUserPrivateKey(credentialsId: "3850069317-0361503742-3394347927", keyFileVariable: 'SSH_KEY')]) {
        sh """
            ssh-keyscan -t rsa bitbucket.org > /usr/share/jenkins/.ssh/known_hosts
            cp ${SSH_KEY} /usr/share/jenkins/.ssh/id_rsa
            chmod 400 /usr/share/jenkins/.ssh/id_rsa
        """
    }
}

def getDisplayName() {
    return "#${BUILD_NUMBER}" + (releasedVersion.isEmpty() ? "" : " - v${releasedVersion}")
}
